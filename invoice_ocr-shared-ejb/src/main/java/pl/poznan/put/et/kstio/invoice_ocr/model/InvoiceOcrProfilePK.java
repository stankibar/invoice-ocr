/* 
 * Copyright (C) 2015  Bartosz Stankiewicz
 *
 * This file is part of Invoice OCR.
 *
 * Invoice OCR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Invoice OCR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Invoice OCR.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.poznan.put.et.kstio.invoice_ocr.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author BartaZ
 */
@Embeddable
public class InvoiceOcrProfilePK implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	private Long nip;
	@NotNull
	private String profile;
	@NotNull
	private Integer rev;
	@NotNull
	private String fieldName;

	public InvoiceOcrProfilePK() {
	}

	public InvoiceOcrProfilePK(Long nip, String profile, Integer rev, String fieldName) {
		this.nip = nip;
		this.profile = profile;
		this.rev = rev;
		this.fieldName = fieldName;
	}

	public Long getNip() {
		return nip;
	}

	public void setNip(Long nip) {
		this.nip = nip;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public Integer getRev() {
		return rev;
	}

	public void setRev(Integer rev) {
		this.rev = rev;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result + (int) (nip ^ (nip >>> 32));
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result + rev;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceOcrProfilePK other = (InvoiceOcrProfilePK) obj;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		if (nip != other.nip)
			return false;
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (rev != other.rev)
			return false;
		return true;
	}

}
