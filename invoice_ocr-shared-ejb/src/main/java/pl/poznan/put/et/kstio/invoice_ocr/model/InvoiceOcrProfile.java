/* 
 * Copyright (C) 2015  Bartosz Stankiewicz
 *
 * This file is part of Invoice OCR.
 *
 * Invoice OCR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Invoice OCR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Invoice OCR.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.poznan.put.et.kstio.invoice_ocr.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author BartaZ
 */
@Entity
@XmlRootElement
@Table(name = "invoice_ocr_profile", uniqueConstraints = @UniqueConstraint(columnNames = {"nip", "profile", "rev", "field_name"}))
public class InvoiceOcrProfile implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@AttributeOverride(name="fieldName", column=@Column(name="field_name"))
	private InvoiceOcrProfilePK profilePk;

	@NotNull
	private Float x;

	@NotNull
	private Float y;

	@NotNull
	private Float width;

	@NotNull
	private Float height;
	private String expression1;
	private String expression2;

	public InvoiceOcrProfile() {
	}

	public InvoiceOcrProfile(InvoiceOcrProfilePK profilePk, Float x, Float y, Float width, Float height, String expression1, String expression2) {
		this.profilePk = profilePk;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.expression1 = expression1;
		this.expression2 = expression2;
	}

	public InvoiceOcrProfilePK getProfilePk() {
		return profilePk;
	}

	public void setProfilePk(InvoiceOcrProfilePK profilePk) {
		this.profilePk = profilePk;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public String getExpression1() {
		return expression1;
	}

	public void setExpression1(String epression1) {
		this.expression1 = epression1;
	}

	public String getExpression2() {
		return expression2;
	}

	public void setExpression2(String epression2) {
		this.expression2 = epression2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expression1 == null) ? 0 : expression1.hashCode());
		result = prime * result + ((expression2 == null) ? 0 : expression2.hashCode());
		result = prime * result + Float.floatToIntBits(height);
		result = prime * result + ((profilePk == null) ? 0 : profilePk.hashCode());
		result = prime * result + Float.floatToIntBits(width);
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceOcrProfile other = (InvoiceOcrProfile) obj;
		if (expression1 == null) {
			if (other.expression1 != null)
				return false;
		} else if (!expression1.equals(other.expression1))
			return false;
		if (expression2 == null) {
			if (other.expression2 != null)
				return false;
		} else if (!expression2.equals(other.expression2))
			return false;
		if (Float.floatToIntBits(height) != Float.floatToIntBits(other.height))
			return false;
		if (profilePk == null) {
			if (other.profilePk != null)
				return false;
		} else if (!profilePk.equals(other.profilePk))
			return false;
		if (Float.floatToIntBits(width) != Float.floatToIntBits(other.width))
			return false;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		return true;
	}

}
