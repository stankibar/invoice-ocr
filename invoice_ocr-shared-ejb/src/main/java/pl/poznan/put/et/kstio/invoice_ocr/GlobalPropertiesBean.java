/* 
 * Copyright (C) 2015  Bartosz Stankiewicz
 *
 * This file is part of Invoice OCR.
 *
 * Invoice OCR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Invoice OCR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Invoice OCR.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.poznan.put.et.kstio.invoice_ocr;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

/**
 *
 * @author BartaZ
 */
@Stateless
public class GlobalPropertiesBean implements GlobalProperies {

	private static final Logger logger = Logger.getLogger(GlobalPropertiesBean.class);
	private Properties globalProperties = null;
	private InputStream is;

	@PostConstruct
	private void init() {
		is = GlobalPropertiesBean.class.getClassLoader().getResourceAsStream("global.properties");
		try {
			globalProperties = new Properties();
			globalProperties.load(is);
		} catch (FileNotFoundException ex) {
			logger.error("FileNotFound: " + ex.getMessage());
		} catch (IOException ex) {
			logger.error("IO: " + ex.getMessage());
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException ex) {
					logger.error(ex.getMessage());
				}
			}
		}
	}

	@Override
	public String getTessDataPrefix() {
		return globalProperties.getProperty("tesseract.data.prefix");
	}

	@Override
	public String getTessDataLanguage() {
		return globalProperties.getProperty("tesseract.data.language");
	}

}
