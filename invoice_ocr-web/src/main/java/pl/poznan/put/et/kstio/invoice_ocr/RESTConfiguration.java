/* 
 * Copyright (C) 2015  Bartosz Stankiewicz
 *
 * This file is part of Invoice OCR.
 *
 * Invoice OCR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Invoice OCR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Invoice OCR.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.poznan.put.et.kstio.invoice_ocr;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("")
public class RESTConfiguration extends Application {

}
