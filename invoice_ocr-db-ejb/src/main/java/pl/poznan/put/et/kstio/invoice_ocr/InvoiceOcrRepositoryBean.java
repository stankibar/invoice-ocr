/* 
 * Copyright (C) 2015  Bartosz Stankiewicz
 *
 * This file is part of Invoice OCR.
 *
 * Invoice OCR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Invoice OCR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Invoice OCR.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.poznan.put.et.kstio.invoice_ocr;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
//import javax.enterprise.event.Event;
//import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;

import pl.poznan.put.et.kstio.invoice_ocr.model.InvoiceOcrProfile;

/**
 *
 * @author BartaZ
 */
@Stateless
public class InvoiceOcrRepositoryBean implements InvoiceOcrRepository {

	@PersistenceContext(unitName = "mainDatasourcePU")
	private EntityManager em;

	@PostConstruct
	private void init() {
		testConnection();
	}

	@Override
	public void register(InvoiceOcrProfile field) {
		// log.log(Level.INFO, "Registering field: {0}",
		// invoiceOcrProfile.getProfilePk().getFieldName());

		// using Hibernate session(Native API) and JPA entitymanager
		Session session = (Session) em.getDelegate();
		session.persist(field);
		// invoiceOcrProfileEventSrc.fire(field);
	}

	@Override
	public void register(List<InvoiceOcrProfile> profile) {
		Integer currentRev = profile.get(0).getProfilePk().getRev();
		String profileName = profile.get(0).getProfilePk().getProfile();
		boolean newRevFlag = currentRev == null;
		Integer maxRevFromDb = getMaxRev(profileName);
		maxRevFromDb++;
		for (InvoiceOcrProfile field : profile) {
			if (newRevFlag)
				field.getProfilePk().setRev(maxRevFromDb);
			register(field);
		}
	}

	@Override
	public boolean testConnection() {
		Session session = (Session) em.getDelegate();
		Object result = session.createSQLQuery("select 1").uniqueResult();
		if (result == null || !(result instanceof Integer)) {
			return false;
		}
		return Integer.valueOf(1).equals((Integer) result);
	}

	@SuppressWarnings("unchecked")
	public List<InvoiceOcrProfile> getMaxRevProfile(String profile_name) {
		Integer maxRev = getMaxRev(profile_name);
		Session session = (Session) em.getDelegate();
		Query q = session.createQuery("from InvoiceOcrProfile where profile = '" + profile_name + "' and rev = " + maxRev);
		return q.list();
	}

	protected Integer getMaxRev(String profile_name) {
		Session session = (Session) em.getDelegate();
		Object result = session.createSQLQuery("select max(rev) from invoice_ocr_profile where profile = '" + profile_name + "'").uniqueResult();
		return (result == null) ? 0 : (Integer) result;
	}

}
