/* 
 * Copyright (C) 2015  Bartosz Stankiewicz
 *
 * This file is part of Invoice OCR.
 *
 * Invoice OCR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Invoice OCR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Invoice OCR.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.poznan.put.et.kstio.invoice_ocr;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import pl.poznan.put.et.kstio.invoice_ocr.model.InvoiceOcrProfile;
import pl.poznan.put.et.kstio.invoice_ocr.model.InvoiceOcrProfilePK;

/**
 * 
 * @author BartaZ
 */
@Stateless
public class PdfProfileBean implements PdfProfile {

	private static final Logger logger = Logger.getLogger(PdfProfileBean.class);

	public PdfProfileBean() {
		logger.debug("PdfProfileBean: constructor");
	}

	@PostConstruct
	private void init() {
		logger.debug("PdfProfileBean: init()");
	}

	@EJB
	private InvoiceOcrRepository ior;

	@Override
	public void saveProfile(File file, Map<String, String> metadata) throws IOException {
		InputStream is = new FileInputStream(file);
		PDDocument pdfDocument = null;
		try {
			pdfDocument = PDDocument.load(is);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new IOException("Problem z ładowaniem pliku PDF");
		}
		if (pdfDocument != null) {
			logger.debug("czytam metadane");
			if (metadata == null)
				throw new IOException("Nie ustawiono metadata");
			String nipString = metadata.get("nip").toString();
			String profileName = metadata.get("profileName").toString();
			if (nipString == null || profileName == null)
				throw new IOException("Nie ustawiono pola nip lub profileName w metadata");
			Long nip = Long.valueOf(nipString);

			logger.debug("czytam adnotacje");
			try {
				PDPage page = (PDPage) pdfDocument.getDocumentCatalog().getAllPages().get(0);
				float pageWidth = page.getMediaBox().getUpperRightX();
				float pageHeight = page.getMediaBox().getUpperRightY();
				List<InvoiceOcrProfile> newProfile = new ArrayList<InvoiceOcrProfile>();
				for (PDAnnotation annotation : page.getAnnotations()) {
					if ("FreeText".equals(annotation.getSubtype())) {
						InvoiceOcrProfile field = new InvoiceOcrProfile();
						field.setProfilePk(new InvoiceOcrProfilePK(nip, profileName, null, annotation.getContents()));
						field.setX(annotation.getRectangle().getLowerLeftX() / pageWidth);
						field.setY(annotation.getRectangle().getLowerLeftY() / pageHeight);
						field.setWidth(annotation.getRectangle().getWidth() / pageWidth);
						field.setHeight(annotation.getRectangle().getHeight() / pageHeight);
						field.setExpression1(annotation.getDictionary().getString("Subj"));
						newProfile.add(field);
					}
				}
				ior.register(newProfile);
			} catch (IOException e) {
				logger.error("Nie udalo sie zaladowac dokumentu albo pobrac pol", e);
				throw new IOException("Nie udalo sie zaladowac dokumentu albo pobrac pol: " + e.getMessage());
			}

			finally {
				if (pdfDocument != null) {
					try {
						pdfDocument.close();
					} catch (IOException e) {
						logger.error("Nie udalo sie zamknac dokumentu pdf", e);
						throw new IOException("Nie udalo sie zamknac dokumentu pdf: " + e.getMessage());
					}
				}
			}
		}
	}

	@Override
	public String getBarcode(File file) throws IOException {
		PDDocument pdfDocument = null;
		try {
			FileInputStream is = new FileInputStream(file);
			pdfDocument = PDDocument.load(is);
		} catch (FileNotFoundException e1) {
			logger.error(e1.getMessage(), e1);
			throw new IOException("Nie ma takiego pliku PDF");
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new IOException("Problem z ładowaniem pliku PDF");
		}

		if (pdfDocument == null)
			throw new IOException("Zmienna pdfDocument jest null'em");

		PDPage page = (PDPage) pdfDocument.getDocumentCatalog().getAllPages().get(0);
		Map<String, PDXObject> pageXObjects = page.getResources().getXObjects();
		if (pageXObjects != null) {
			BufferedImage bufferedImage = null;
			for (PDXObject value : pageXObjects.values()) {
				if (value instanceof PDXObjectImage) {
					bufferedImage = ((PDXObjectImage) value).getRGBImage();
					break;
				}
			}
			if (bufferedImage != null) {
				Reader reader = new MultiFormatReader();
				BinaryBitmap image = new BinaryBitmap(
						new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
				try {
					Map<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();
					hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
					Result result = reader.decode(image, hints);
					return result.getText();
				} catch (NotFoundException e) {
					logger.error(e.getMessage(), e);
				} catch (ChecksumException e) {
					logger.error(e.getMessage(), e);
				} catch (FormatException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return null;
	}
}
