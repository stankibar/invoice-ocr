/* 
 * Copyright (C) 2015  Bartosz Stankiewicz
 *
 * This file is part of Invoice OCR.
 *
 * Invoice OCR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Invoice OCR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Invoice OCR.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.poznan.put.et.kstio.invoice_ocr.test;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author BartaZ
 */
public class PdfBoxTest {

	private static final Logger logger = Logger.getLogger(PdfBoxTest.class);
    private static File pdf;

    public PdfBoxTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    	pdf = new File("C:\\Users\\BartaZ\\Desktop\\faktury\\Faktura_20130621_18081489_szablon2.pdf");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    @Ignore
    public void hello() {
	PDDocument pdfDocument = null;
	try {
	    pdfDocument = PDDocument.load(pdf);
	    PDPage page = (PDPage) pdfDocument.getDocumentCatalog().getAllPages().get(0);
	    page.getMediaBox().getUpperRightX();
	    page.getMediaBox().getUpperRightY();
	    System.out.println("ArtBox: " + page.getArtBox());
	    System.out.println("BleedBox: " + page.getBleedBox());
	    System.out.println("CropBox: " + page.getCropBox());
	    System.out.println("MediaBox: " + page.getMediaBox().getUpperRightX() + ", " + page.getMediaBox().getUpperRightY());
	    System.out.println("TrimBox: " + page.getTrimBox());
	    for (COSName key : page.getCOSDictionary().keySet()) {
		System.out.println("COSDict name: " + key.getName() + " value: " + page.getCOSDictionary().getItem(key));
	    }
	    for (PDAnnotation annotation : page.getAnnotations()) {
		if ("FreeText".equals(annotation.getSubtype())) {
		    System.out.println("Contents: " + annotation.getContents());
		    System.out.println("LowerLeftX: " + annotation.getRectangle().getLowerLeftX());
		    System.out.println("LowerLeftY: " + annotation.getRectangle().getLowerLeftY());
		    System.out.println("UpperRightX: " + annotation.getRectangle().getUpperRightX());
		    System.out.println("UpperRightY: " + annotation.getRectangle().getUpperRightY());
		    System.out.println("Rectangle: " + annotation.getRectangle().toString());
		    for (COSName key : annotation.getDictionary().keySet()) {
			System.out.println("Dict name: " + key.getName() + " value: " + annotation.getDictionary().getString(key));
		    }
		}
	    }

	} catch (IOException ex) {
		System.err.println(ex.getMessage());
	} finally {
	    if (pdfDocument != null) {
		try {
		    pdfDocument.close();
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
	    }
	}
    }
    
    @Test
    public void testPageXObjects() {
    	PDDocument pdfDocument = null;
    	try {
    	    pdfDocument = PDDocument.load(pdf);
    	    PDPage page = (PDPage) pdfDocument.getDocumentCatalog().getAllPages().get(0);
    	    Map<String, PDXObject> pageXObjects = page.getResources().getXObjects();
    		if (pageXObjects != null) {
    			Set<String> keys = pageXObjects.keySet();
    			for (String key : keys) {
    				PDXObject el = pageXObjects.get(key);
    				logger.info(key + " = " + el.getClass() + " is image: " + (el instanceof PDXObjectImage));
    				if (el instanceof PDXObjectImage)
    					((PDXObjectImage) el).getRGBImage();
    			}
    		}
    	} catch(IOException e) {
    		System.err.println(e.getMessage());
    	}
    }

//    @Test
//    public void hello2() {
//	List<IIOImage> pdfImageList = null;
//	try {
//	    
//	    pdfImageList = ImageIOHelper.getIIOImageList(pdf);
//	    IIOImage pdfImage = pdfImageList.get(0);
//	    RenderedImage bi = pdfImage.getRenderedImage();
//	    System.out.println("Width: " + bi.getWidth());
//	    System.out.println("Height: " + bi.getHeight());
//	} catch (IOException ex) {
//	    Logger.getLogger(PdfBoxTest.class.getName()).log(Level.SEVERE, null, ex);
//	}
//    }
}
